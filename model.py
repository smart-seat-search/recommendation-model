import numpy as np


def euclidian_dist(src_vect, dst_vect) -> float:
    return np.sqrt(sum(np.sqr(src_vect - dst_vect) * weights))

def manhattan_dist(src_vect, weights, dst_vect) -> float:
    return sum(abs(src_vect - dst_vect) * weights)


class RecomendationModel:
    _distance_functions = {'euclidian': euclidian_dist, 'manhattan': manhattan_dist}
    _max_bags = 12 # 3 bags for 4 people

    def __init__(self, distance_metric: str):
        if distance_metric in self._distance_functions:
            self.metric = self._distance_functions[distance_metric]

    def _calculate_clusters_dist(self, src_vect: dict, weights: list, dst_clust: dict) -> float:
        src = np.array(list(src_vect.values()))
        wght = np.array(weights)
        dst_clust_encode = [self._encode_answers(clust) for clust in dst_clust]
        
        dst = {}
        
        for key in src_vect.keys():
            dst[key] = 0
            for elem in dst_clust_encode:
                for key_dst in elem.keys():
                    dst[key] += elem[key_dst]
            
            if key != "bagsNumber" and key != "tableUsage":
                dst[key] /= len(dst_clust_encode)
            elif key == "bagsNumber":
                dst[key] = self._max_bags - dst[key]
            else:
                dst[key] = 0.5 * (len(dst_clust_encode) + 1) - dst[key] * len(dst_clust_encode)
                
        dst = np.array(list(dst_clust_encode[0].values()))

        return self.metric(src, wght, dst)

    def _encode_answers(self, client_attr: dict) -> dict:
        dict_transform = {
            "Ж" : 0, "М" : 1,
            "Нет" : 0, "Да" : 1,
            "Жарко" : 0, "Холодно" : 1,
            "Не важно" : 0.5,
            "Читать": 0, "Спать": 1,
            "Болтать": 2, "Использовать свои гаджеты": 3,
            "Ничего" : 1.5,
            "Жаворонок" : 0, "Сова" : 1,
            "Опционально" : 0.5,
            "Активно" : 1, "Не использую" : 0.2,
            "Периодически" : 0.5
        }
        transform_attr = {}
        for key, val in client_attr.items():
            if key == "age" or key == "bagsNumber":
                transform_attr[key] = val    
            elif key != "seat":
                transform_attr[key] = dict_transform[val]
        
        return transform_attr

    def _encode_weights(self, client_weights: list) -> list:
        dict_encode_weights = {
            "Очень важно" : 1,
            "Нейтрально" : 0.5,
            "Не важно" : 0.2
        }

        transform_weights = []
        for elem in client_weights:
            transform_weights += [dict_encode_weights[elem]]

        return transform_weights

    def _filter_seats(self, train: dict, train_res: dict) -> dict:
        res_dict = {}
        for num_coupe, clients in train.items():
            if len(clients) != 4:
                number_seats = [4 * num_coupe - i for i in range(4)]
                for client in clients:
                    number_seats.remove(client["seat"])
                for seat in number_seats:
                    res_dict[seat] = train_res[num_coupe]

        return res_dict
    
    def predict(self, client: dict, client_weights: list, all_coupe: dict) -> dict:
        distances_to_src = {}
        client_encode = self._encode_answers(client)
        weights_encode = self._encode_weights(client_weights)
        weights_encode = [1, 1] + weights_encode
        weights_encode.insert(-1, 1)
        
        for coupe_num, coupe in all_coupe.items():
            
            distances_to_src[coupe_num] = (
                self._calculate_clusters_dist(client_encode, weights_encode, coupe)
            )

        # calculate max num
        max_distances_for_params = np.array([1, 100, 1, 0.5, 1, 1, 4, 12, 1])
        max_dist = sum(np.array(weights_encode) * max_distances_for_params) # max(distances_to_src.values())
        res_dict = dict(map(lambda tup: (tup[0], (1 - (tup[1] / max_dist)) * 100),
                            distances_to_src.items()))

        return self._filter_seats(all_coupe, res_dict)
